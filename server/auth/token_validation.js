const { verify } = require("jsonwebtoken")

module.exports = {
    checkToken: (req, res, next) => {
        let token = req.get("authorization")
        if (token) {
            if (token.startsWith('Bearer ')) {
                token = token.slice(7)
            }
            verify(token, process.env.JWT_PRIVATE_KEY, (err, decoded) => {
                if (err) {
                    res.status(400).json({
                        success: false,
                        message: "Invalid Token."
                    })
                } else {
                    req.user = decoded
                    next()
                }
            })
        } else {
            res.status(200).json({
                success: false,
                message: "Access Denied! Unauthorized user."
            })
        }
    }
}