require("dotenv").config()
const cors = require("cors")

const express = require("express")
const app = express()
const userRouter = require("./api/users/user.router")

app.use(express.json())
app.use(cors())

app.use("/api/users", userRouter)
app.listen(process.env.APP_PORT, () => {
    console.log("Server up and running on PORT : ", process.env.APP_PORT)
})