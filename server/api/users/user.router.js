const user_controller = require("./user.controller")
const router = require("express").Router()
const { checkToken } = require("../../auth/token_validation")

router.post("/register", user_controller.createUser)
router.post("/login", user_controller.login)
router.post("/resendOTP", user_controller.resendOTP)
router.post("/verifyOTP", user_controller.verifyOTP)

router.get("/", checkToken, user_controller.getUser)
router.patch("/", checkToken, user_controller.updateUser)
router.delete("/", checkToken, user_controller.deleteUser)

module.exports = router