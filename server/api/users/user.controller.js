const { genSaltSync, hashSync, compareSync } = require("bcrypt")
const { sign } = require("jsonwebtoken")
const user_service = require("./user.service")

module.exports = {
    createUser: async (req, res) => {
        const body = req.body
        if (!body.email || !body.password) {
            return res.status(200).json({
                success: false,
                message: "Missing API parameter, please contact support."
            })
        }

        if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(body.password)) {
            return res.status(200).json({
                success: false,
                message: "Password must contain at least 8 characters, 1 num, lower and uppercase letters & special characters."
            })
        }

        if (await user_service.isExist(body.email)) {
            return res.status(200).json({
                success: false,
                message: `User ${body.email} already exist.`
            })
        } else {
            const salt = genSaltSync(10)
            body.password = hashSync(body.password, salt)

            await user_service.createUser(body, async (err, results) => {
                if (err) {
                    console.log(err)
                    return res.status(500).json({
                        success: false,
                        message: "Server internal error, unable to create " + body.email
                    })
                }

                await user_service.sendOTP(body.email, (err, results) => {
                    if (err) {
                        console.log(err)
                        return res.status(500).json({
                            success: false,
                            message: "Server internal error, unable to send OTP."
                        })
                    }

                    return res.status(200).json({
                        success: true,
                        message: "OTP sent, please check your email to verify your account."
                    })
                })
            })
        }
    },
    // getUserById: (req, res) => {
    //     const id = req.params.id
    //     getUserById(id, (err, results) => {
    //         if (err) {
    //             console.log(err)
    //             return
    //         }
    //         if (!results) {
    //             return res.json({
    //                 sucess: 0,
    //                 message: "Record not found."
    //             })
    //         }
    //         return res.json({
    //             success: 1,
    //             message: results
    //         })
    //     })
    // },
    getUser: async (req, res) => {
        var email = req.user.result.email
        await user_service.getUser(email, (err, results) => {
            if (err) {
                console.log(err)
                return res.status(500).json({
                    success: false,
                    message: `Server internal error, unable to find ${email} details.`
                })
            }
            if (!results) {
                return res.status(200).json({
                    sucess: false,
                    message: "Record not found."
                })
            }
            return res.status(200).json({
                success: true,
                message: results
            })
        })
    },
    updateUser: async (req, res) => {
        const body = req.body
        const email = req.user.result.email
        if (!body.password) {
            return res.status(200).json({
                success: false,
                message: "Missing API parameter, please contact support."
            })
        }

        const salt = genSaltSync(10)
        body.password = hashSync(body.password, salt)
        await user_service.updateUser(req, (err, results) => {
            if (err) {
                console.log(err)
                return res.status(500).json({
                    success: false,
                    message: "Server internal error, unable to update " + email
                })
            }
            if (results.affectedRows == 0) {
                return res.status(200).json({
                    success: false,
                    message: `User ${email} not found.`
                })
            }
            return res.status(200).json({
                sucess: true,
                message: "Successfully Updated."
            })
        })
    },
    deleteUser: async (req, res) => {
        const email = req.user.result.email

        await user_service.deleteUser(req, (err, results) => {
            if (err) {
                console.log(err)
                return res.status(500).json({
                    success: false,
                    message: "Server internal error, unable to delete " + email
                })
            }
            if (results.affectedRows == 0) {
                return res.status(200).json({
                    success: false,
                    message: `User ${email} not found.`
                })
            }
            return res.status(200).json({
                sucess: true,
                message: "Successfully Deleted."
            })
        })
    },
    login: async (req, res) => {
        const body = req.body;
        if (!body.email || !body.password) {
            return res.status(200).json({
                success: false,
                message: "Missing API parameter, please contact support."
            })
        }

        await user_service.getUser(body.email, (err, results) => {
            if (err) {
                console.log(err);
                return res.status(500).json({
                    success: false,
                    message: "Server internal error, unable to find " + email
                })
            }
            if (!results) {
                return res.status(200).json({
                    success: false,
                    message: "Invalid email or password."
                })
            }

            if (!results.isVerified) {
                return res.status(200).json({
                    success: false,
                    message: "Account has not been validated."
                })
            }

            const correctPass = compareSync(body.password, results.password)
            if (correctPass) {
                results.password = undefined
                const jsontoken = sign({ result: results }, process.env.JWT_PRIVATE_KEY, {
                    expiresIn: "1h"
                })
                return res.status(200).json({
                    success: true,
                    message: "Login Successful.",
                    token: jsontoken
                })
            } else {
                return res.status(200).json({
                    success: false,
                    message: "Invalid email or password"
                })
            }
        })
    },
    resendOTP: async (req, res) => {
        const email = req.body.email
        if (!email) {
            return res.status(200).json({
                success: false,
                message: "Missing API parameter, please contact support."
            })
        }

        if (await user_service.isExist(email)) {
            return res.status(200).json({
                success: false,
                message: `User ${email} already verified.`
            })
        } else {
            await user_service.sendOTP(email, (err, results) => {
                if (err) {
                    console.log(err)
                    return res.status(500).json({
                        success: false,
                        message: "Server internal error, unable to send OTP."
                    })
                }

                return res.status(200).json({
                    success: true,
                    message: "OTP sent, please check your email to verify your account."
                })
            })
        }
    },
    verifyOTP: async (req, res) => {
        const body = req.body
        if (!body.email) {
            return res.status(200).json({
                success: false,
                message: "Missing API parameter, please contact support."
            })
        }

        if (await user_service.isExist(body.email)) {
            return res.status(200).json({
                success: false,
                message: `User ${body.email} already verfied.`
            })
        } else {
            await user_service.getOTP(body.email, async (err, results) => {
                if (err) {
                    console.log(err);
                    return res.status(500).json({
                        success: false,
                        message: "Server internal error, unable to verify " + email
                    })
                }

                if (results.length == 0) {
                    return res.status(200).json({
                        success: false,
                        message: "OTP is no longer valid."
                    })
                } else if (results.otp == body.otp) {
                    await user_service.setVerified(req, (err, results) => {
                        if (err) {
                            console.log(err)
                            return res.status(500).json({
                                success: false,
                                message: "Server internal error, unable to verify " + email
                            })
                        }
                        if (results.affectedRows == 0) {
                            return res.status(200).json({
                                success: false,
                                message: `User ${email} not found.`
                            })
                        }
                        return res.status(200).json({
                            sucess: true,
                            message: "Successfully Verified."
                        })
                    })
                } else {
                    return res.status(200).json({
                        success: false,
                        message: "Incorrect OTP."
                    })
                }
            })
        }
    }
}