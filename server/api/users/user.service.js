const pool = require("../../config/database")
const nodemailer = require('nodemailer')
const { google } = require('googleapis')

const oAuth2Client = new google.auth.OAuth2(
    process.env.GOOGLE_CLIENT_ID,
    process.env.GOOGLE_CLIENT_SECRET,
    process.env.REDIRECT_URI
)

oAuth2Client.setCredentials({ refresh_token: process.env.REFRESH_TOKEN })

module.exports = {
    isExist: async (email) => {
        try {
            var results = await pool.query(
                `SELECT *
                    FROM User
                    WHERE email=?
                    AND isVerified=1`,
                [email]
            )

            if (results.length == 0) {
                // user does not exist
                return false
            } else {
                // user exist
                return true
            }
        } catch (error) {
            return error
        }

    },
    createUser: (data, callBack) => {
        try {
            var results = pool.query(
                `INSERT INTO User(email, password)
                    VALUES(?, ?)
                    ON DUPLICATE KEY UPDATE password=?`,
                [
                    data.email,
                    data.password,
                    data.password
                ]
            )
            return callBack(null, results)
        } catch (error) {
            return callBack(error)
        }
    },
    getUser: async (email, callBack) => {
        try {
            var results = await pool.query(
                `SELECT *
                    FROM User 
                    WHERE email = ?`,
                [email]
            )
            return callBack(null, results[0])
        } catch (error) {
            return callBack(error)
        }

    },
    // getUserById: (id, callBack) => {
    //     pool.query(
    //         `SELECT *
    //             FROM User
    //             WHERE id=?`,
    //         [id],
    //         (error, results, fields) => {
    //             if (error) {
    //                 return callBack(error)
    //             }
    //             return callBack(null, results[0])
    //         }
    //     )
    // },
    updateUser: async (req, callBack) => {
        const data = req.body
        try {
            var results = await pool.query(
                `UPDATE User
                    SET password=?
                    WHERE email=?`,
                [
                    data.password,
                    req.user.result.email
                ]
            )
            return callBack(null, results)
        } catch (error) {
            return callBack(error)
        }
    },
    deleteUser: async (req, callBack) => {
        try {
            var results = await pool.query(
                `DELETE FROM User
                    WHERE email=?`,
                [req.user.result.email]
            )
            return callBack(null, results)
        } catch (error) {
            return callBack(error)
        }
    },
    getUserByEmail: (email, callBack) => {
        pool.query(
            `SELECT *
                FROM User
                WHERE email=?`,
            [email],
            (error, results, fields) => {
                if (error) {
                    return callBack(error)
                }
                return callBack(null, results[0])
            }
        )
    },
    sendOTP: async (email, callBack) => {
        var otp = Math.random()
        otp = Math.floor(100000 + Math.random() * 900000)

        try {
            // save otp in DB
            var results = pool.query(
                `INSERT INTO Otp(email, otp)
                    VALUES(?, ?)
                    ON DUPLICATE KEY UPDATE otp=?`,
                [
                    email,
                    otp,
                    otp
                ]
            )

            const accessToken = await oAuth2Client.getAccessToken()

            const transport = nodemailer.createTransport({
                service: "gmail",
                auth: {
                     type: "OAuth2",
                     user: process.env.EMAIL_ADDRESS, 
                     clientId: process.env.GOOGLE_CLIENT_ID,
                     clientSecret: process.env.GOOGLE_CLIENT_SECRET,
                     refreshToken: process.env.REFRESH_TOKEN,
                     accessToken: accessToken
                }
           });

            var mailOptions = {
                from: '"Relog Team" <from@relog.com>',
                to: email,
                subject: 'Relog OTP Verification',
                text: 'Hey there! This is your OTP for your Relog account: ' + otp,
                html: '<b>Hey there!</b><br>This is the OTP for your Relog account:<br><h1>'+ otp +'</h1>'
            }

            transport.sendMail(mailOptions, (error, info) => {
                if (error) {
                    return callBack(error)
                } else {
                    console.log('Message sent: %s', info.messageId)
                    return callBack(null, results)
                }
            })
        } catch (error) {
            return callBack(error)
        }
    },
    getOTP: async (email, callBack) => {
        try {
            var results = await pool.query(
                    `SELECT *
                    FROM Otp
                    WHERE email=?
                    AND otp_timestamp > DATE_sub(NOW(), INTERVAL 10 MINUTE)`,
                [
                    email
                ]
            )
            results = JSON.parse(JSON.stringify(results))
            return callBack(null, results[0])
        } catch (error) {
            return callBack(error)
        }
    },
    setVerified: async (req, callBack) => {
        const data = req.body
        try {
            var results = await pool.query(
                `UPDATE User
                    SET isVerified=?
                    WHERE email=?`,
                [
                    1,
                    data.email
                ]
            )
            return callBack(null, results)
        } catch (error) {
            return callBack(error)
        }
    }
}