import React from "react"
import {
  Button,
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label
} from "reactstrap"
import { Mail } from "react-feather"
import "../../assets/scss/pages/authentication.scss"
import updateImg from "../../assets/img/pages/forgot-password.png"
import { history } from "../../history"
import Axios from "axios"

class Delete extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: 0,
      email: "",
      message: ""
    }

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(e) {
    e.preventDefault()

    let message = ""

    if (this.state.email === "") {
      message += "Please enter email to confirm account deletion."
    } else if (this.state.email !== localStorage.getItem("email")) {
      message += "Email does not match."
    }

    if (message === "") {
      let api = process.env.REACT_APP_API_ENDPOINT + "/api/users/"
      Axios.delete(api, {
        headers: {
          "authorization": localStorage.getItem("token")
        }
      }).then((res) => {
        if (res.data.success) {
          localStorage.clear()
          history.push("/login")
        } else {
          this.setState({ message: res.data.message })
        }
      })
    } else {
      this.setState({ message: message })
    }
  }

  render() {
    let message = ''
    if (this.state.message) {
      if (this.state.message.includes("Successfully")) {
        message = <div className="alert alert-success" role="alert">
          <p>{this.state.message}</p>
        </div>
      } else {
        message = <div className="alert alert-warning" role="alert">
          <p>{this.state.message}</p>
        </div>
      }
    }
    return (
      <Card>
        <CardHeader><CardTitle>Delete Account</CardTitle></CardHeader>

        <CardBody>
          <Row className="m-0">
            <Col lg="6" md="12" className="p-0">
              <Form onSubmit={this.handleSubmit}>
                <FormGroup className="form-label-group position-relative has-icon-left">
                  <Input
                    type="email"
                    placeholder="Email"
                    value={this.state.email}
                    onChange={e => this.setState({ email: e.target.value })}
                  />
                  <div className="form-control-position">
                    <Mail size={15} />
                  </div>
                  <Label>Email</Label>
                </FormGroup>
                {message}
                <div className="d-flex justify-content-between">
                  <Button.Ripple color="danger" type="submit">
                    Delete
                  </Button.Ripple>
                </div>
              </Form>
            </Col>
            <Col lg="6" className="d-lg-block d-none text-center align-self-center px-1 py-0">
              <img src={updateImg} alt="updateImg" />
            </Col>
          </Row>

        </CardBody>
      </Card>
    )
  }
}

export default Delete