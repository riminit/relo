import React from "react"
import {
  Button,
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label
} from "reactstrap"
import { Mail, Lock } from "react-feather"
import "../../assets/scss/pages/authentication.scss"
import updateImg from "../../assets/img/pages/forgot-password.png"
import Axios from "axios"

class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email: "",
      password: "",
      confirmPass: "",
      message: ""
    }

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount() {
    this.setState({email: localStorage.getItem("email")})
  }

  handleSubmit(e) {
    e.preventDefault()

    let message = ""

    if (this.state.password === "") {
      message += "Need a password!"
    } else if (this.state.password !== this.state.confirmPass) {
      message += "Password does not match!"
    } else if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(this.state.password)) {
      message += "Password must contain at least 8 characters, 1 num, lower and uppercase letters & special characters."
    }

    if (message === "") {
      let api = process.env.REACT_APP_API_ENDPOINT + "/api/users/"
      Axios.patch(api, {
        email: this.state.email,
        password: this.state.password
      }, {
        headers: {
          "authorization": localStorage.getItem("token")
        }
      }).then((res) => {
        this.setState({ message: res.data.message })
      })
    } else {
      this.setState({ message: message })
    }
  }

  render() {
    let message = ''
    if (this.state.message) {
      if (this.state.message.includes("Successfully")) {
        message = <div className="alert alert-success" role="alert">
          <p>{this.state.message}</p>
        </div>
      } else {
        message = <div className="alert alert-warning" role="alert">
          <p>{this.state.message}</p>
        </div>
      }
    }
    return (
      <Card>
        <CardHeader><CardTitle>Your Profile</CardTitle></CardHeader>

        <CardBody>
          <Row className="m-0">
            <Col lg="6" md="12" className="p-0">
              <Form onSubmit={this.handleSubmit}>
                <FormGroup className="form-label-group position-relative has-icon-left">
                  <Input
                    type="email"
                    placeholder="Email"
                    value={this.state.email}
                    disabled
                  />
                  <div className="form-control-position">
                    <Mail size={15} />
                  </div>
                  <Label>Email</Label>
                </FormGroup>
                <FormGroup className="form-label-group position-relative has-icon-left">
                  <Input
                    type="password"
                    placeholder="Password"
                    value={this.state.password}
                    onChange={e => this.setState({ password: e.target.value })}
                  />
                  <div className="form-control-position">
                    <Lock size={15} />
                  </div>
                  <Label>Password</Label>
                </FormGroup>
                <FormGroup className="form-label-group position-relative has-icon-left">
                  <Input
                    type="password"
                    placeholder="Confirm Password"
                    value={this.state.confirmPass}
                    onChange={e => this.setState({ confirmPass: e.target.value })}
                  />
                  <div className="form-control-position">
                    <Lock size={15} />
                  </div>
                  <Label>Confirm Password</Label>
                </FormGroup>
                {message}
                <div className="d-flex justify-content-between">
                  <Button.Ripple color="primary" type="submit">
                    Update Profile
                  </Button.Ripple>
                </div>
              </Form>
            </Col>
            <Col lg="6" className="d-lg-block d-none text-center align-self-center px-1 py-0">
              <img src={updateImg} alt="updateImg" />
            </Col>
          </Row>

        </CardBody>
      </Card>
    )
  }
}

export default Home