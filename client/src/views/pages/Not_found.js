import React from "react"
import {
  Card,
  CardBody,
  Row,
  Col,
} from "reactstrap"
import img from "../../assets/img/pages/404.png"


class Not_Found extends React.Component {
  render() {
    return (
      <Row className="m-0 justify-content-center">
        <Col
          sm="8"
          xl="7"
          lg="10"
          md="8"
          className="d-flex justify-content-center"
        >
          <Card className="bg-authentication login-card rounded-0 mb-0 w-100">
            <Row className="m-0">
              <Col
                lg="6"
                className="d-lg-block d-none text-center align-self-center px-1 py-0"
              >
                <br></br>
                <img src={img} alt="loginImg" />
                <br></br>
                <br></br>
              </Col>
              <Col lg="6" md="12" className="p-0">
                <Card className="rounded-0 mb-0 px-2">
                  <CardBody>
                    <h4>404 ERROR</h4>
                    <p>The page your looking for does not exist.</p>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>


      // <Row className="m-0 justify-content-center">
      //   <Col sm="6 justify-content-center">
      //     <h1>404: Not Found</h1>
      //     <p>The page your looking for does not exist.</p>
      //   </Col>
      // </Row>
    )
  }
}

export default Not_Found