import React from "react"
import {
  Button,
  Card,
  CardBody,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label
} from "reactstrap"
import { Mail, Lock, Check } from "react-feather"
import { history } from "../../../../history"
import Checkbox from "../../../../components/@vuexy/checkbox/CheckboxesVuexy"

import loginImg from "../../../../assets/img/pages/login.png"
import "../../../../assets/scss/pages/authentication.scss"

import Axios from "axios"

class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      //activeTab: "1",
      email: "",
      password: "",
      message: ""
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  // toggle = tab => {
  //   if (this.state.activeTab !== tab) {
  //     this.setState({
  //       activeTab: tab
  //     })
  //   }
  // }

  handleSubmit(e) {
    e.preventDefault()
    let api = process.env.REACT_APP_API_ENDPOINT + "/api/users/login"
    Axios.post(api, {
      email: this.state.email,
      password: this.state.password
    }).then((res) => {
      console.log(res)
      if (res.data.success) {
        localStorage.setItem("token", res.data.token)
        localStorage.setItem("email", this.state.email)

        this.setState({ message: "" })
        history.push("/")
      } else {
        this.setState({ message: res.data.message })
      }
    })
  }

  render() {
    let message = ''
    if (this.state.message) {
      message = <div className="alert alert-warning" role="alert">
                  <p>{this.state.message}</p>
                </div>
    }
    return (
      <Row className="m-0 justify-content-center">
        <Col
          sm="8"
          xl="7"
          lg="10"
          md="8"
          className="d-flex justify-content-center"
        >
          <Card className="bg-authentication login-card rounded-0 mb-0 w-100">
            <Row className="m-0">
              <Col
                lg="6"
                className="d-lg-block d-none text-center align-self-center px-1 py-0"
              >
                <img src={loginImg} alt="loginImg" />
              </Col>
              <Col lg="6" md="12" className="p-0">
                <Card className="rounded-0 mb-0 px-2">
                  <CardBody>
                    <h4>Login</h4>
                    <p>Welcome back, please login to your account.</p>
                    <Form onSubmit={this.handleSubmit}>
                      <FormGroup className="form-label-group position-relative has-icon-left">
                        <Input
                          type="email"
                          placeholder="Email"
                          value={this.state.email}
                          onChange={e => this.setState({ email: e.target.value })}
                        />
                        <div className="form-control-position">
                          <Mail size={15} />
                        </div>
                        <Label>Email</Label>
                      </FormGroup>
                      <FormGroup className="form-label-group position-relative has-icon-left">
                        <Input
                          type="password"
                          placeholder="Password"
                          value={this.state.password}
                          onChange={e => this.setState({ password: e.target.value })}
                        />
                        <div className="form-control-position">
                          <Lock size={15} />
                        </div>
                        <Label>Password</Label>
                      </FormGroup>
                      {message}
                      <FormGroup className="d-flex justify-content-between align-items-center">
                        <Checkbox
                          color="primary"
                          icon={<Check className="vx-icon" size={16} />}
                          label="Remember me"
                        />
                        <div className="float-right">
                          Forgot Password?
                            </div>
                      </FormGroup>
                      <div className="d-flex justify-content-between">
                        <Button.Ripple color="primary" outline onClick={() => history.push("/register")}>
                          Register
                        </Button.Ripple>
                        <Button.Ripple color="primary" type="submit">
                          Login
                        </Button.Ripple>
                      </div>
                    </Form>
                  </CardBody>
                  {/* <div className="auth-footer">
                        <div className="divider">
                          <div className="divider-text">OR</div>
                        </div>
                        <div className="footer-btn">
                          <Button.Ripple className="btn-facebook" color="">
                            <Facebook size={14} />
                          </Button.Ripple>
                          <Button.Ripple className="btn-twitter" color="">
                            <Twitter size={14} stroke="white" />
                          </Button.Ripple>
                          <Button.Ripple className="btn-google" color="">
                            <img src={googleSvg} alt="google" height="15" width="15" />
                          </Button.Ripple>
                          <Button.Ripple className="btn-github" color="">
                            <GitHub size={14} stroke="white" />
                          </Button.Ripple>
                        </div>
                      </div> */}
                </Card>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    )
  }
}
export default Login
