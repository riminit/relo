import React from "react"
import {
    Button,
    Card,
    CardBody,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Label
} from "reactstrap"
import { Mail, Lock } from "react-feather"
import { history } from "../../../history"

import registerImg from "../../../assets/img/pages/register.jpg"
import "../../../assets/scss/pages/authentication.scss"

import Axios from "axios"

class Register extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            email: "",
            password: "",
            confirmPass: "",
            message: "",
            isOtpPage: false,
            otp: ""
        }
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleSubmit(e) {
        e.preventDefault()

        let message = ""

        if (!this.state.isOtpPage) {
            if (this.state.email === "") {
                message += "<li>Need an email address!</li>"
            }

            if (this.state.password === "") {
                message += "<li>Need a password!</li>"
            } else if (this.state.password !== this.state.confirmPass) {
                message += "<li>Password does not match!</li>"
            } else if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(this.state.password)) {
                message += "<li>Password must contain at least 8 characters, 1 num, lower and uppercase letters & special characters.</li>"
            }

            if (message === "") {
                let api = process.env.REACT_APP_API_ENDPOINT + "/api/users/register"
                Axios.post(api, {
                    email: this.state.email,
                    password: this.state.password
                }).then((res) => {
                    console.log(res)
                    if (res.data.success) {
                        this.setState({ message: message, isOtpPage: true })
                        this.render()
                    } else {
                        this.setState({ message: res.data.message })
                    }
                })
            } else {
                this.setState({ message: message })
            }
        } else {
            if (this.state.otp === "") {
                message += "Need an OTP."
            }

            if (message === "") {
                let api = process.env.REACT_APP_API_ENDPOINT + "/api/users/verifyOTP"
                Axios.post(api, {
                    email: this.state.email,
                    otp: this.state.otp
                }).then((res) => {
                    console.log(res)
                    this.setState({ message: res.data.message })
                })
            } else {
                this.setState({ message: message })
            }
        }
    }

    resendOTP(e) {
        e.preventDefault()
        let api = process.env.REACT_APP_API_ENDPOINT + "/api/users/resendOTP"
        Axios.post(api, {
            email: this.state.email
        }).then((res) => {
            console.log(res)
            this.setState({ message: res.data.message })
        })
    }

    render() {
        let message = ''
        if (this.state.message) {
            if (this.state.message === "Successfully Verified.") {
                message = <div className="alert alert-success" role="alert">
                    <p>{this.state.message}</p>
                </div>
            } else if (!this.state.message.startsWith("<li>")) {
                message = <div className="alert alert-warning" role="alert">
                    <p>{this.state.message}</p>
                </div>
            } else {
                message = <div className="alert alert-warning" role="alert">
                    <ul dangerouslySetInnerHTML={{ __html: this.state.message }} />
                </div>
            }
        }
        if (!this.state.isOtpPage) {
            return (
                <Row className="m-0 justify-content-center">
                    <Col
                        sm="8"
                        xl="7"
                        lg="10"
                        md="8"
                        className="d-flex justify-content-center"
                    >
                        <Card className="bg-authentication login-card rounded-0 mb-0 w-100">
                            <Row className="m-0">
                                <Col
                                    lg="6"
                                    className="d-lg-block d-none text-center align-self-center px-1 py-0"
                                >
                                    <img src={registerImg} alt="registerImg" />
                                </Col>
                                <Col lg="6" md="12" className="p-0">
                                    <Card className="rounded-0 mb-0 px-2">
                                        <CardBody>
                                            <h4>Register</h4>
                                            <p>Welcome to Relog, please enter your account details.</p>
                                            <Form onSubmit={this.handleSubmit}>
                                                <FormGroup className="form-label-group position-relative has-icon-left">
                                                    <Input
                                                        type="email"
                                                        placeholder="Email"
                                                        value={this.state.email}
                                                        onChange={e => this.setState({ email: e.target.value })}
                                                    />
                                                    <div className="form-control-position">
                                                        <Mail size={15} />
                                                    </div>
                                                    <Label>Email</Label>
                                                </FormGroup>
                                                <FormGroup className="form-label-group position-relative has-icon-left">
                                                    <Input
                                                        type="password"
                                                        placeholder="Password"
                                                        value={this.state.password}
                                                        onChange={e => this.setState({ password: e.target.value })}
                                                    />
                                                    <div className="form-control-position">
                                                        <Lock size={15} />
                                                    </div>
                                                    <Label>Password</Label>
                                                </FormGroup>
                                                <FormGroup className="form-label-group position-relative has-icon-left">
                                                    <Input
                                                        type="password"
                                                        placeholder="Confirm Password"
                                                        value={this.state.confirmPass}
                                                        onChange={e => this.setState({ confirmPass: e.target.value })}
                                                    />
                                                    <div className="form-control-position">
                                                        <Lock size={15} />
                                                    </div>
                                                    <Label>Confirm Password</Label>
                                                </FormGroup>
                                                {message}
                                                <div className="d-flex justify-content-between">
                                                    <Button.Ripple color="primary" outline onClick={() => history.push("/login")}>
                                                        Return to Login
                                                    </Button.Ripple>
                                                    <Button.Ripple color="primary" type="submit">
                                                        Register
                                                    </Button.Ripple>
                                                </div>
                                            </Form>
                                        </CardBody>
                                    </Card>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            )
        } else {
            return (
                <Row className="m-0 justify-content-center">
                    <Col
                        sm="8"
                        xl="7"
                        lg="10"
                        md="8"
                        className="d-flex justify-content-center"
                    >
                        <Card className="bg-authentication login-card rounded-0 mb-0 w-100">
                            <Row className="m-0">
                                <Col
                                    lg="6"
                                    className="d-lg-block d-none text-center align-self-center px-1 py-0"
                                >
                                    <img src={registerImg} alt="registerImg" />
                                </Col>
                                <Col lg="6" md="12" className="p-0">
                                    <Card className="rounded-0 mb-0 px-2 py-4">
                                        <CardBody>
                                            <h4>Verify Account</h4>
                                            <p>Please check your email for the one-time password.</p>
                                            <Form onSubmit={this.handleSubmit}>
                                                <FormGroup className="form-label-group position-relative has-icon-left">
                                                    <Input
                                                        type="number"
                                                        placeholder="One-Time Password"
                                                        value={this.state.otp}
                                                        onChange={e => this.setState({ otp: e.target.value })}
                                                    />
                                                    <div className="form-control-position">
                                                        <Lock size={15} />
                                                    </div>
                                                    <Label>One-Time Password</Label>
                                                </FormGroup>
                                                {message}
                                                <FormGroup className="d-flex justify-content-between align-items-center">
                                                    <div className="float-right">
                                                        Did not receive otp? <a href="#" onClick={(e) => this.resendOTP(e)}>Send Again!</a>
                                                    </div>
                                                </FormGroup>
                                                <div className="d-flex justify-content-between">
                                                    <Button.Ripple color="primary" outline onClick={() => history.push("/login")}>
                                                        Return to Login
                                                    </Button.Ripple>
                                                    <Button.Ripple color="primary" type="submit">
                                                        Verify
                                                    </Button.Ripple>
                                                </div>
                                            </Form>
                                        </CardBody>
                                    </Card>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            )
        }

    }
}
export default Register
