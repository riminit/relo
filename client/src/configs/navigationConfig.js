import React from "react"
import * as Icon from "react-feather"
const navigationConfig = [
  {
    id: "home",
    title: "Home",
    type: "item",
    icon: <Icon.Home size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/"
  },
  {
    id: "delete",
    title: "Delete Account",
    type: "item",
    icon: <Icon.Trash size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/delete"
  }
]

export default navigationConfig
