import React, { Suspense, lazy } from "react"
import { Router, Switch, Route, Redirect } from "react-router-dom"
import { history } from "./history"
import { connect } from "react-redux"
import Spinner from "./components/@vuexy/spinner/Loading-spinner"
import { ContextLayout } from "./utility/context/Layout"

// Route-based code splitting
const Home = lazy(() =>
  import("./views/pages/Home")
)

const Delete = lazy(() =>
  import("./views/pages/Delete")
)

const login = lazy(() =>
  import("./views/pages/authentication/login/Login")
)

const register = lazy(() =>
  import("./views/pages/authentication/Register")
)

const not_found = lazy(() =>
  import("./views/pages/Not_found")
)

// Set Layout and Component Using App Route
const RouteConfig = ({
  component: Component,
  fullLayout,
  permission,
  user,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => {
      return (
        <ContextLayout.Consumer>
          {context => {
            let LayoutTag =
              fullLayout === true
                ? context.fullLayout
                : context.state.activeLayout === "horizontal"
                ? context.horizontalLayout
                : context.VerticalLayout
              return (
                <LayoutTag {...props} permission={props.user}>
                  <Suspense fallback={<Spinner />}>
                    <Component {...props} />
                  </Suspense>
                </LayoutTag>
              )
          }}
        </ContextLayout.Consumer>
      )
    }}
  />
)

const ProtectedRouteConfig = ({
  component: Component,
  fullLayout,
  permission,
  user,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => {
      if (localStorage.getItem("token")) {
        return (
          <ContextLayout.Consumer>
            {context => {
              let LayoutTag =
                fullLayout === true
                  ? context.fullLayout
                  : context.state.activeLayout === "horizontal"
                  ? context.horizontalLayout
                  : context.VerticalLayout
                return (
                  <LayoutTag {...props} permission={props.user}>
                    <Suspense fallback={<Spinner />}>
                      <Component {...props} />
                    </Suspense>
                  </LayoutTag>
                )
            }}
          </ContextLayout.Consumer>
        )
      } else {
        return <Redirect to={
          {
            pathname: "/login",
            state: {
              from: props.location
            }
          }
        }/>
      }
    }}
  />
)

const mapStateToProps = state => {
  return {
    user: state.auth.login.userRole
  }
}

const AppRoute = connect(mapStateToProps)(RouteConfig)
const ProtectedRoute = connect(mapStateToProps)(ProtectedRouteConfig)

class AppRouter extends React.Component {
  render() {
    return (
      // Set the directory path if you are deploying in sub-folder
      <Router history={history}>
        <Switch>
          <ProtectedRoute
            exact
            path="/"
            component={Home}
          />
          <ProtectedRoute
            exact
            path="/delete"
            component={Delete}
          />
          <AppRoute
            exact
            path="/login"
            component={login}
            fullLayout
          />
          <AppRoute
            exact
            path="/register"
            component={register}
            fullLayout
          />
          <AppRoute
            path="*"
            component={not_found}
            fullLayout
          />
        </Switch>
      </Router>
    )
  }
}

export default AppRouter
